% asciipat(6) | User Commands
%
% "June 20 2023"

# NAME

asciipat - A retro ASCII art game inspired by Moon Patrol.

# SYNOPSIS

**asciipat** [option] [.rec file]

# DESCRIPTION

**asciipat** is a command-line game that brings back the nostalgia
of retro ASCII art games. It is inspired by the classic Moon Patrol
game. Guide your character through a treacherous terrain, avoiding
obstacles and shooting down enemies. Survive as long as possible to
achieve a high score.

After finishing game you may notice there is "asciipat.rec" file in
your home directory. It can be used to replay game or to validate your
score.

This manual page was written for the Debian distribution because the
original program does not have a manual page. Instead, it has documentation
in the GNU info(1) format; see below.

# OPTIONS

**0** [.rec file]
:  Score validation (no graphics) - this is used by hi-scores server.

**1** [.rec file]
:  Time lapsed (fast) playback.

**2** [.rec file]
:  Normal playback - Used it to capture videos.

# FILES

${HOME}/asciipat.cfg
:   This file is created when you change your player name, avatar, or
    key bindings.

${HOME}/asciipat.rec
:   This file is created when you start playing the game, and get a
    score.
    
# ENVIRONMENT

**ASCII_PATROL_CONFIG_DIR **
:   If used, the defined directory is used to store the asciipat.cfg (see also
    the section called “FILES”).

**ASCII_PATROL_RECORD_DIR **
:   If used, the defined directory is used to store the asciipat.rec (see also
    the section called “FILES”).
    
# BUGS

The program is currently limited to workaround some issues please refer to the following
upstream Problems section at https://github.com/msokalski/ascii-patrol/tree/1.7#problems

# SEE ALSO

For more details about the options see what the author shared at
http://ascii-patrol.com/forum/index.php?topic=63

# AUTHOR

Raul Cheleguini <raul.cheleguini@gmail.com>
:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2023 Raul Cheleguini

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU General Public License, Version 3 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.

[comment]: #  Local Variables:
[comment]: #  mode: markdown
[comment]: #  End:
