Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/msokalski/ascii-patrol
Upstream-Name: ascii-patrol
Upstream-Contact: msokalski / https://twitter.com/mrgumix
Files-Excluded:
 html5
 temp_xp
Comment:
 html5: Web port has a minified JS with no source, thus removed.
 temp_xp: MS Windows port files.
  
Files: *
Copyright: 2017-2023 msokalski / https://twitter.com/mrgumix
License: GPL-3+

Files:
 stb_vorbis.cpp
Copyright: Copyright 2017 Sean Barrett
License: public-domain and Expat

Files:
 debian/*
Copyright:
 2023 Raul Cheleguini <raul.cheleguini@gmail.com>
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
Comment:
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: public-domain
 I, Sean Barrett, single-handedly wrote the decoder, and as the legal author (it
 was not a "work for hire") placed it in the public domain in April, 2007. That
 means I have disclaimed copyright of it. You can take the source code and use
 it for any purpose, commercial or non-commercial, copyrighted, open-source,
 Free, free, or public domain, with no legal obligation to acknowledge the
 borrowing/copying in any way.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
